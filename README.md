This is deployed in Vercel.
Deployment process:
- created account on Vercel
- Connecting gitlab to vercel.
- after that import project from gitlab to vercel.
- then deploy.
Project Link: https://xmail-gilt.vercel.app/

To run locally Installing process:
1. Install nodejs
2. after downloading this app open in any text editor(ex: vscode) then in command line write npm run
------------------------------
* Xmail
This is a mail service web app built using react and firebase where one user can send mail to another user.

* Workings
1. User can create account 
2. User can log in by already created account.
3. User can not register with already register email.
4. User can receive email in four category.

* Screen Shot.
1. As a user, You can sign up with your first name, last name, new email, and password. The email address must be unique.
![email_already_used](/uploads/b61f05b41d395036f91f8c6723556a03/email_already_used.png)

2. As a user, You can sign in using your email address and password.
![emaildontexist](/uploads/3b65d785717d6d35ac2472d49a16859e/emaildontexist.png)

![passnotmatch](/uploads/1634256a4e9aa9a5ee0a2c50e3805b57/passnotmatch.png)

3.  As a user, You can send emails to another email address.
![compose](/uploads/cc0dfd06ae623014e719d53dc4b734c3/compose.png)

4. As a user, You can receive emails from another email address.
![view_mail](/uploads/9154ea15a9411fccee6372659d2a0837/view_mail.png)

5. As a user, You will get emails in your inbox in four categories: Primary, Social, Promotional, and Forum. Emails are automatically categorized.
![forum](/uploads/c80ed4d8de84feba96be4dd1f34082d6/forum.png)

![primary](/uploads/ac4ce496c167b996c8d3dc0816d2cc1d/primary.png)

![promotion](/uploads/c574466b04a76dee9fab73c856dec059/promotion.png)

![social](/uploads/e1c57b627fa745fb9d519bba024de59b/social.png)
>>>>>>> README.md
