import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyD9blbwpQZlDEfP9MMXjCShcNletNKIFyM',
  authDomain: 'xmail-96ccd.firebaseapp.com',
  projectId: 'xmail-96ccd',
  storageBucket: 'xmail-96ccd.appspot.com',
  messagingSenderId: '690302829004',
  appId: '1:690302829004:web:2f08b1927716dfaac15d69',
};

firebase.initializeApp(firebaseConfig);
export const auth = firebase.auth();
export const db = firebase.firestore();
